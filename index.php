<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('include/head.php') ?>
</head>
<body>
<?php include('include/header.php') ?>

<section class="walnut_mind_home_background">
	<div class="walnut_mind_home_bg_image">
		<div class="container">
			<div class="row">
				<div class="col-6">	
				 <div class="walnut_video_container right_p top_p">
	   				<video playsinline autoplay muted loop>
		    		 <source src="video/1.mp4" type="video/mp4">
	   				</video>
     				<div class="overlay-desc">
        				<h1>INDUSTRIA</h1>
                  <div class="walnut_boxdetail">
                      <a href="storelist.php"><button>Start Shopping</button></a>
                  </div>
     				</div>
				</div>
				</div>
			<div class="col-6">
				 <div class="walnut_video_container left_p top_p">
	   				<video playsinline autoplay muted loop>
		    		 <source src="video/2.mp4" type="video/mp4">
	   				</video>
     				<div class="overlay-desc">
        				<h1>GREENHILLS</h1>
                <div class="walnut_boxdetail">
                      <a href="storelist.php"><button>Start Shopping</button></a>
                  </div>
     				</div>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12" id="content">
					<h1 style="font-family: 'Dancing Script', cursive;">Welcome</h1>
					 <div class="circle blue"></div>
				</div>
			</div>
			<div class="row walnut_tb">
				<div class="col-6">
				 <div class="walnut_video_container right_p">
	   				<video playsinline autoplay muted loop>
		    		 <source src="video/3.mp4" type="video/mp4">
	   				</video>
     				<div class="overlay-desc">
        				<h1>ESTANCIA</h1>
                <div class="walnut_boxdetail">
                      <a href="storelist.php"><button>Start Shopping</button></a>
                  </div>
     				</div>
				</div>
				</div>
				<div class="col-6">
				 <div class="walnut_video_container left_p">
	   				<video playsinline autoplay muted loop>
		    		 <source src="video/4.mp4" type="video/mp4">
	   				</video>
     				<div class="overlay-desc">
        				<h1>TIENDESITAS</h1>
                <div class="walnut_boxdetail">
                      <a href="storelist.php"><button>Start Shopping</button></a>
                  </div>
     				</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="banner-padding">
	<div class="banner">
		<p>Walkommerce Mall</p>
	</div>
</section>

<section>
	<div class="container">
		 <div class="row mb-5">
		 	<div class="col-6">
		 		<div class="walnut_vendorbox walnut_backgroundvendor">
          <h1>Fashion</h1>
			</div>
		 	</div>

		 			<div class="col-6">
		 		<div class="walnut_vendorbox walnut_backgroundvendortwo">
          <h1>Watch</h1>
			</div>
		 	</div>
		 </div>

		 	<div class="row mt-5">
				<div class="col-12" id="content">
					<h1 class="walnut_just_mall">Just One Click into Digital Mall</h1>
				</div>
			</div>



		 		<div class="row walnut_digital_tb mt-5">
		 		<div class="col-6">
		 		<div class="walnut_vendorbox walnut_backgroundvendorthree">
          <h1>Mobile</h1>
			</div>
		 	</div>
		 		<div class="col-6">
		 		<div class="walnut_vendorbox walnut_backgroundvendorfour">
          <h1>Women</h1>
			</div>
		 	</div>
		 	</div>
		 </div>
	
</section>

<hr>
<section class="top_deal">
	<div class="walnut_head_title">
		<h1 style="font-family: 'Arvo', serif;">Top Products</h1>
	</div>
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-12 shop">
	<div id="owl-demo3">
  	<div class="item brand"><img src="images/zara.jpg" alt="Shirt">
  	</div>
  <div class="item brand"><img src="images/croma.jpg" alt="Shirt">
  </div>
    	<div class="item brand"><img src="images/nike.jpg" alt="Shirt">
  	</div>
</div>
</div>
</div>
</div>
</section>
<section class="top_deal">
	<div class="walnut_head_title">
		<h1 style="font-family: 'Arvo', serif;"> Latest Deal</h1>
	</div>
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-12">
	<div id="owl-demo">
  	<div class="item">
      <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	<div class="page-wrapper">
  	<button id="addtocart" class="addme">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
	</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>

    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	<div class="page-wrapper">
  	<button id="addtocartt">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/4.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocarttt">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartttt">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocarttttt">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartttttt">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocarttttttt">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartttttttt">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  
</div>
</div>
</div>
</div>
</section>

<section class="top_deal">
	<div class="walnut_head_title">
		<h1 style="font-family: 'Arvo', serif;"> Hot Deal</h1>
	</div>
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-12">
	<div id="owl-demo2">
  	<div class="item">
      <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
      <img src="product/5.jpg" alt="Shirt">
       <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	<div class="page-wrapper">
  	<button id="addtocartone">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
	</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	<div class="page-wrapper">
  	<button id="addtocarttwo">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/4.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartthree">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartfour">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/5.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartfive">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartsix">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartseven">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocarteight">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  
</div>
</div>
</div>
</div>
</section>
<hr>
<section class="trending_discount">
	<div class="walnut_head_title">
		<h1 style="font-family: 'Arvo', serif;">Trending</h1>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<div class="trend">
					<img src="product/trend.jpg">
				</div>
			</div>
			<div class="col-md-3">
				<div class="trend">
					<img src="images/trendtwo.jpg">
				</div>
			</div>
			<div class="col-md-6">
				<div class="discount_banner">
					<img src="images/discount.jpg">
				</div>
			</div>
		</div>
	</div>
</section>


<section class="top_deal">
	<div class="walnut_head_title">
		<h1 style="font-family: 'Arvo', serif;">Loyalty Offers</h1>
	</div>
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-12">
	<div id="owl-demo5">
  	<div class="item">
      <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
      <img src="product/5.jpg" alt="Shirt">
       <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	<div class="page-wrapper">
  	<button id="addtocartonee">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
	</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	<div class="page-wrapper">
  	<button id="addtocarttwoo">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/4.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartthreee">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartfourr">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/5.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartfivee">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartsixx">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocartsevenn">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
  	 	<div class="page-wrapper">
  	<button id="addtocarteightt">
    Add to Cart
    <span class="cart-item"></span>
  	</button>
</div>
  </div>
  
</div>
</div>
</div>
</div>
</section>

<section class="ban">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<div class="cover">
					<img src="images/cover.jpg">
				</div>
			</div>
			<div class="col-md-6">
				<div class="cover">
					<img src="images/cover2.jpg">
				</div>
			</div>
		</div>
	</div>
</section>


<?php include('include/footer.php') ?>
</body>
</html>

