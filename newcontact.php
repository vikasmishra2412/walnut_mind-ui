 <!doctype html>
<html lang="en">
  <head>

    <?php include('include/head.php') ?>
    
  </head>
  <body>
    <?php include('include/header.php') ?>

    

    <div class="container">
      <div class="row contactRow"> 
       <div class="col-sm-12 col-md-12 col-lg-6">
          <div class="title newtitle walnut_head_title">
    <h1>Contact us</h1>
  </div>
  <div class="form">
    <div class="form-items">
      <input type="text" class="input" placeholder="Username">
      <i class="fas fa-user"></i>
    </div>
    <div class="form-items">
      <input type="text" class="input" placeholder="Email">
      <i class="fas fa-envelope"></i>
    </div>
    <div class="form-items">
      <textarea class="input message" cols="30" rows="10" placeholder="Message....."></textarea>
    </div>
  </div>
  
  <div class="newConBtn">
    Submit
    <i class="fas fa-arrow-right"></i>
  </div>
  
  
  <div class="social-icons">
    <div class="facebook">
      <i class="fab fa-facebook-f ConFb"></i>
    </div>
    <div class="twitter">
      <i class="fab fa-twitter conTwit"></i>
    </div>
    <div class="google">
      <i class="fab fa-google-plus-g"></i>
    </div>
  </div>
       </div>
       <div class="col-sm-12 col-md-12 col-lg-6">
         <!--Google map-->
          <div class="newConMap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3771.5154827791043!2d73.02451321472917!3d19.041060387107937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c3c53b1e255b%3A0x3fa8b73a42118233!2sD%20Y%20Patil%20Sports%20Stadium!5e0!3m2!1sen!2sin!4v1604062856873!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
          </div>
<!--Google Maps-->
       </div>
      </div>
    </div>


    <?php include('include/footer.php') ?>
  </body>
</html>