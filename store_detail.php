<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('include/head.php') ?>
</head>
<body>
<?php include('include/header.php') ?>

<section>
  <div class="walnut_store_detail_banner">
    <img src="images/hmban.jpg"  class="w-100">
  </div>
</section>
<section class="walnut_filter">
  <div class="container">
    <div class="row mt-4">
      <div class="col-md-9">
        <div class="example">
        <form>
        <input type="search" class="searchbox walnut_box" name="q" autocomplete="off" placeholder="Search Your Product..." />
        <button type="submit" class="searchbutton"><i class="fa fa-search"></i></button>
        </form>
        </div>
      </div>
      <div class="col-md-3">
       <div class="walnut_filtr_button">
        <button>Filter</button>
       </div>
      </div>
    </div>
  </div>
</section>

<section class="walnut_store_detail">
  <div class="container-fluid">
    <div class="row">
      <div class="col-6 col-lg-5">
        <div class="walnut_store_banner">
          <img src="images/hmcover1.jpg">
        </div>
      </div>
      <div class="col-6 col-lg-5">
        <div class="walnut_store_banner">
          <img src="images/hmcover2.jpg">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="Arrival">
  <div class="walnut_head_title">
    <h1 style="font-family: 'Arvo', serif;">New Arrival</h1>
  </div>
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12">
  <div id="owl-demo">
    <a href="product_details.php">
    <div class="item">
      <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocart" class="addme">
    Add to Cart
    <span class="cart-item"></span>
    </button>
  </div>
  </div>
</a>
  <a href="product_details.php">
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>

    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocartt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
</a>
<a href="product_details.php">
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/4.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocarttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
</a>
<a href="product_details.php">
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocartttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
</a>
<a href="product_details.php">
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocarttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
</a>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocartttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocarttttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocartttttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  
</div>
</div>
</div>
</div>
</section>

<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="walnut_store_detail_product_adv">
          <img src="images/storedetail.jpg">
        </div>
      </div>
    </div>
  </div>
</section>


<section class="Latest">
  <div class="walnut_head_title">
    <h1 style="font-family: 'Arvo', serif;"> Latest Deal</h1>
  </div>
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12">
  <div id="owl-demo2">
    <div class="item">
      <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
      <img src="product/5.jpg" alt="Shirt">
       <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocartone">
    Add to Cart
    <span class="cart-item"></span>
    </button>
  </div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocarttwo">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/4.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocartthree">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocartfour">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/5.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocartfive">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocartsix">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocartseven">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
     <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
      <div class="page-wrapper">
    <button id="addtocarteight">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  
</div>
</div>
</div>
</div>
</section>

<section class="offer_sec_walnut">
  <div class="walnut_head_title">
    <h1 style="font-family: 'Arvo', serif;">Latest Offer</h1>
  </div>
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12 offer">
  <div id="owl-demo3">
    <div class="item"><img src="images/10.png" alt="Shirt">
    </div>
  <div class="item"><img src="images/30.png" alt="Shirt">
  </div>
      <div class="item"><img src="images/50.png" alt="Shirt">
    </div>
</div>
</div>
</div>
</div>
</section>




<?php include('include/footer.php') ?>
<script src="js/main.js"></script>
</body>
</html>

