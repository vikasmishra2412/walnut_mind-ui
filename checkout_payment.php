<!doctype html>
<html lang="en">
  <head>
    <?php include('include/head.php') ?>
  </head>
  <body>
    <?php include('include/header.php') ?>

    <div class="container">
        <div class="walnut_timelineNav">
            <div class="wallnut_timelineOption"><a href="" class="wallnut_timleineText">Address</a></div>
            <div class="walnut_timelineLine"></div>
            <div class="wallnut_timelineOption"><a href="" class="wallnut_timleineText">Order</a></div>
            <div class="walnut_timelineLine"></div>
            <div class="wallnut_timelineOptionActive"><a href="" class="wallnut_timleineText">Payment</a></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-8  wallnut_addressForm">
        <h3>Shipping Information</h3>
        <div class="wallnut_dividerLine"></div>
         <div class="wn_customerDetail">
            <h5>Name : </h5>
            <h5> Walkcommerce</h5>
         </div>
        <div class="wn_customerDetail">
            <h5>Location : </h5>
            <h5> Banglore</h5>
         </div>
         <div class="wn_customerDetail">
            <h5>Contact : </h5>
            <h5> 800900100</h5>
         </div>
         <div class="wn_customerDetail">
            <h5>Email : </h5>
            <h5> Walkommerce@gmail.com</h5>
         </div>
        <h3 class="wn_paymentinfo">Payment Information</h3>
        <div class="wallnut_dividerLine"></div>
        <div class="form-check wallnut_radioTxt">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">
            <label class="form-check-label" for="exampleRadios1">
            Cash On Delivery
            </label>
        </div>
        <div class="form-check wallnut_radioTxt">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">
            <label class="form-check-label" for="exampleRadios1">
            Razorpay
            </label>
        </div>
        <div class="form-check wallnut_radioTxt">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">
            <label class="form-check-label" for="exampleRadios1">
            Mobile Money
            </label>
        </div>
        
        <div class="wallnut_checkBtns">
             <a class="wallnut_checkoutBtn wn_bakBtn" href="checkout_order.php">Back</a>
             <a class="wallnut_checkoutBtn" href="thankyou.php"> Continue</a>
        </div>
    </div><!--col-->

    <!-- *****Loyalty Pricing Details Section***** -->
        <div class="col col-xs-12 price_details">
            <!-- col-sm-12 col-md-12 col-lg-4 -->
            <div class="mt-2 mb-3 pt-2 loTitle">
            <h5>PRICE DETAILS</h5>
            </div>
            <div class="row loRow price_detailsHead">
                <div class="col-6">
                    <h5 class="float-left loH5">Total MRP</h5>
                    
                </div>
                <div class="col-6">
                    <h5 class="float-right loH5">&#x20B9;5941.2</h5>
                    
                </div>
                <div class="col-6">
                    <h5 class="float-left loH5">Shipping</h5>
                </div>
                <div class="col-6">
                    <h5 class="float-right loH5">&#x20B9;40</h5>
                </div>
            </div>
            <div class="row loRow mt-3 price_detailsTotal">
                <div class="col-6">
                    <h5 class="float-left loH5">Total</h5>
                </div>
                <div class="col-6">
                    <h5 class="float-right loH5">&#x20B9;5941.2</h5>
                </div>
            </div>
            <div class="row ">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h5 class="text_lo"> HAVE A PROMOTION CODE? </h5>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h5 class="text_lo">USE LOYALTY POINT?</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <input class="wn_Lip" type="text" name="" placeholder="Points"><a class="wn_applyBtn" href="#">APPLY</a>
                    <h6 class="ptText">Maximum 59 point for this order</h6>
                </div>
            </div>
            <div class="loTitle">
            <h5>SHIPPING METHOD</h5>
            </div>
             <div class="row loRow">
                <div class="loRadio col-sm-12 col-md-12 col-lg-12">
                    <input type="radio" name=""><label><h5 class="loH5">Free Shipping +&#x20B9;10 </h5></label>
                   <div class="row">
                    <div class="losmall col-sm-12 col-md-12 col-lg-12">
                        <h6>(10 - 20 days)</h6>
                    </div>    
                    </div>
                </div>
                <div class="loRadio col-sm-12 col-md-12 col-lg-12">

                    <input type="radio" name=""><label><h5 class="loH5">Express Shipping +&#x20B9;10 </h5></label> 
                    <div class="row">
                     <div class="losmall col-sm-12 col-md-12 col-lg-12">
                        <h6>(5 - 6 days)</h6>
                     </div>
                    </div>
                </div>
            </div>
            <div class="loTitle">
            <h5>PACKAGING</h5>
            </div>
             <div class="row loRow price_detailsHead">
                <div class="loRadio col-sm-12 col-md-12 col-lg-12">
                    <input type="radio" name=""><label><h5 class="loH5">Defaulf Packaging </h5></label> 
                    <div class="row">
                     <div class="losmall col-sm-12 col-md-12 col-lg-12">
                        <h6>Default Packaging by store</h6>
                     </div>
                    </div>
                </div>
                <div class="loRadio col-sm-12 col-md-12 col-lg-12">
                    <input type="radio" name=""><label><h5 class="loH5">Gift Packaging</h5></label> 
                    <div class="row">
                     <div class="losmall col-sm-12 col-md-12 col-lg-12">
                        <h6>Exclusive Gift packaging</h6>
                     </div>
                    </div> 
                </div>
            </div>
            <div class="wn_loDivider"></div>
            <div class="row loRow mt-3">
                <div class="col-6">
                    <h5 class="loH5 float-left">Final Price :</h5>
                </div>
                <div class="col-6">
                    <h5 class="loH5 float-right">&#x20B9;5951.20</h5>
                </div>
            </div>
            <div class="row loRow">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <p class="float-left mt-3">Loyalty Points : 594</p>
                </div>
            </div>
        </div>
    </div><!-- row-->
    </div><!--conatiner end -->












    <?php include('include/footer.php') ?>
   
  </body>
</html>