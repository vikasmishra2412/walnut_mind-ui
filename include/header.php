<header>
	<div class="walnut_mind_home_head walnut_orange">
		<ul>
     <li><a href=""><i class="fab fa-facebook"></i></a></li>  
     <li><a href=""><i class="fab fa-twitter"></i></a></li>
     <li><a href=""><i class="fab fa-instagram"></i></a></li>
    </ul>
	</div>
	<nav>
		<div class="container-fluid walnut_menu_height">
		<div class="row">
			<div class="col-md-3">
				<div class="walnut_mind_logo">
					<a href="index.php"><img src="logo/logo.png"></a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="walnut_mind_home_search_category walnut_orange">
					<div class="row">
						<div class="col-md-6 col-12 search_right_border">
							<div class="search">
								  <form class="walnut_mind_home_search_container">
    							<input type="text" id="walnut_mind_search_bar" placeholder="What can I help you with today?">
    							<a href="#"><img class="walnut_mind_search_icon" src="http://www.endlessicons.com/wp-content/uploads/2012/12/search-icon.png"></a>
  								</form>
							</div>
						</div>
						<div class="col-md-6">
							<div class="category">
								<span class="walnut_mind_dropdown_main_category">
								<input type="radio" name="sortType" value="Relevance" checked="checked" id="sort-relevance"><label for="sort-relevance" style="color: #fff;">Category</label>
								<input type="radio" name="sortType" value="Popularity" id="sort-best"><label for="sort-best">Product Popularity</label>
								<input type="radio" name="sortType" value="PriceIncreasing" id="sort-low"><label for="sort-low">Price Low to High</label>
								<input type="radio" name="sortType" value="PriceDecreasing" id="sort-high"><label for="sort-high">Price High to Low</label>
								<input type="radio" name="sortType" value="ProductBrand" id="sort-brand"><label for="sort-brand">Product Brand</label>
								<input type="radio" name="sortType" value="ProductName" id="sort-name"><label for="sort-name">Product Name</label>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-1">
			<div class="walnut_cartstyle">
			<div id="walnut_cart" class="walnut_cart" data-totalitems="0">			
      		<a href="#" id="cart"><img src="icon/cart.png" data-cart-toggle></a>
      		<div class="shopping-cart">
        <div class="shopping-cart-header itemp">
          <h2>Items : (1)</h2>
          <div class="shopping-cart-total">
            <span class="lighter-text">Total:</span>
            <span class="main-color-text total">Rs. 6000/-</span>
          </div>
        </div> <!--end shopping-cart-header -->

        <ul class="shopping-cart-items">
          <li class="clearfix">
            <img src="product/4.jpg" alt="Shirt">
            <span class="item-name">Product 1</span>
            <span class="item-price">Rs. 2000/-</span>
            <span class="item-quantity">Quantity: 1</span>
          </li>
          <div class="wallnut_dividerLine"></div>
           <li class="clearfix">
            <img src="product/5.jpg" alt="Shirt">
            <span class="item-name">Product 1</span>
            <span class="item-price">Rs. 2000/-</span>
            <span class="item-quantity">Quantity: 1</span>
          </li>
          <div class="wallnut_dividerLine"></div>
           <li class="clearfix">
            <img src="product/6.jpg" alt="Shirt">
            <span class="item-name">Product 1</span>
            <span class="item-price">Rs. 2000/-</span>
            <span class="item-quantity">Quantity: 1</span>
          </li>
        </ul>

        <a href="cart.php" class="buttonv">View Cart </a> <a href="checkout.php" class="buttonv">Checkout </a>
      </div> <!--end shopping-cart -->
  </div>
  </div>
</div>
	<div class="col-1 p-0">
				<div class="walnut_whislist">
					<div id="walnut_ex4">
  					<i class="icon-heart-empty"></i>
  					</span>
					</div>
				</div>
			</div>
			<div class="col-1">
				<div class="walnut_user">
					<div class="walnut_user_area">
  					<div class="Walnut_user_avatar">
   					<a href="signuploginpg.php"><img src="icon/user.png"/></a>
  				</div>
    			<ul class="Walnut_user_dropdown">
      				<li><a href="#">My Profile</a></li>
      				<li><a href="#">Settings</a></li>
      				<li><a href="#">Logout</a></li>
    			</ul>
				</div>
				</div>
			</div>
</div>
</div>
</nav>
		
	<nav class="navbar navbar-expand-lg walnut_bg_color walnut_orange">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <!-- <span class="navbar-toggler-icon"></span> -->
    <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  	 <div class="Location">
		<span class="walnut_mind_dropdown_main_location">
		<input type="radio" name="sortType" value="Relevance" checked="checked" id="sort-relevance"><label for="sort-relevance" style="color: #fff;">Location</label>
		<input type="radio" name="sortType" value="Popularity" id="sort-best"><label for="sort-best">India</label>
		<input type="radio" name="sortType" value="PriceIncreasing" id="sort-low"><label for="sort-low">U.S.A</label>
		<input type="radio" name="sortType" value="PriceDecreasing" id="sort-high"><label for="sort-high">U.K</label>
		<input type="radio" name="sortType" value="ProductBrand" id="sort-brand"><label for="sort-brand">Russia</label>
		<input type="radio" name="sortType" value="ProductName" id="sort-name"><label for="sort-name">Philippines</label>
		</span>
	</div>
    <ul class="navbar-nav walnut_nav_margin_left">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about.php">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"  data-toggle="modal" data-target="#exampleModal">Track Order</a>
      </li>
     <li class="nav-item">
        <a class="nav-link" href="newcontact.php">Contact</a>
      </li>
    </ul>
   
  </div>
</nav>
</header>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Track Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="container">
   		<div class="row">
   			<div class="col-9">
        <input class="wallnut_loginInput" type="" name="" placeholder="Enter Tracking Code">
        </div>
        <div class="col-3 trackbtn">
        <a class="wallnut_loginPgBtn" href="trackingtimeline.php">View Tracking</a>
        </div>
       </div> 
      </div>
     
    </div>
  </div>
</div>
<script>
	$(document).ready(function(){
(function () {
  $(document).click(function () {
    var $item = $(".shopping-cart");
    if ($item.hasClass("active")) {
      $item.removeClass("active");
    }
  });

  $(".shopping-cart").each(function () {
    var delay = $(this).index() * 50 + "ms";
    $(this).css({
      "-webkit-transition-delay": delay,
      "-moz-transition-delay": delay,
      "-o-transition-delay": delay,
      "transition-delay": delay
    });
  });
  $("#cart").click(function (e) {
    e.stopPropagation();
    $(".shopping-cart").toggleClass("active");
  });

  $("#addtocart").click(function (e) {
    e.stopPropagation();
    $(".shopping-cart").toggleClass("active");
  });
})();

});
</script>