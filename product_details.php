<!doctype html>
<html lang="en">
  <head>
    <?php include('include/head.php') ?>

  </head>
  <body>
    <?php include('include/header.php') ?>

    	
    <section class="wn_productdetails">
        <div class="row pd_row">
        <div class="col-lg-5 wn_productImg">    
      <div class="large-5 column">
        <div class="xzoom-container">
          <img class="xzoom" id="xzoom-default" src="product/4.jpg" xoriginal="product/4.jpg" />
          <div class="xzoom-thumbs">
            <a href="product/4.jpg"><img class="xzoom-gallery" width="80" src="product/8.jpg"  xpreview="product/8.jpg" ></a>    
            <a href="product/4.jpg"><img class="xzoom-gallery" width="80" src="product/4.jpg"  xpreview="product/4.jpg" ></a>   
            <a href="product/8.jpg"><img class="xzoom-gallery" width="80" src="product/8.jpg"  xpreview="product/8.jpg" ></a>  
            <a href="product/4.jpg"><img class="xzoom-gallery" width="80" src="product/4.jpg"  xpreview="product/4.jpg" ></a>
          </div>
        </div>
         <div class="wn_prodBtn">
            <a class="wn_prodDetBtn" href="">Add to Cart</a>
            <a class="wn_prodDetBtn yel" href="">Buy Now</a>
            </div>        
      </div>
      <div class="large-7 column"></div>
        </div>
        <div class="col-lg-5 wn_prodDetails detail">
            <h3 class="text-left">Linen shirt Regular Fit</h3>
            <div class="wn_star">
            <i class="fa fa-check wn_prodStock checkIoc" aria-hidden="true"></i>
            <h4 class="wn_prodStock rm">In Stock</h4>
            <span class="fa fa-star checked starColor rm"></span>
            <span class="fa fa-star checked starColor"></span>
            <span class="fa fa-star checked starColor"></span>
            <span class="fa fa-star checked starColor"></span>
            </div>
            <div class="text-left">
            <h4>Price:  &#x20B9; 2000/-</h4>
            <h5>Available Offres</h5>
            <ul class="wn_offerList">
             <li>Bank offer 5% Unlimited Cashback on Wallkommerce Axis Bank Credit Card</li>
             <li>Bank offer 5% off with Axis Bank Buzz Credit Card</li>
             <li>Bank offer 5% Unlimited Cashback on Wallkommerce Axis Bank Credit Card</li>
             <li>Bank offer 5% off with Axis Bank Buzz Credit Card</li>
            </ul>
            </div>
            <div class="wn_pro">
                <p>Color :</p>
                <div class="wn_color darkblue rm"></div>
                <div class="wn_color yellow"></div>
                <div class="wn_color green"></div>
                <div class="wn_color red"></div>
                <div class="wn_color skyblue"></div>
            </div>
            <div class="wn_pro">
                <p>Size :</p>
                <div class="wn_prodSize rm">XS</div>
                <div class="wn_prodSize">S</div>
                <div class="wn_prodSize">L</div>
                <div class="wn_prodSize">XL</div>
                <div class="wn_prodSize">XXL</div>
            </div>
            <div class="wn_pro">
                <p>Seller :</p>
                <p class="rm">H & M</p>
            </div>
        </div>
        </div>
    </section>

  <section class="top_deal">
    <div class="walnut_head_title">
        <h1 style="font-family: 'Arvo', serif;"> Recommended product</h1>
    </div>
    <div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
    <div id="product_recom">
    <div class="item">
      <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocart" class="addme">
    Add to Cart
    <span class="cart-item"></span>
    </button>
    </div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>

    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocartt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/4.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  
</div>
</div>
</div>
</div>
</section>
<section class="top_deal">
    <div class="walnut_head_title">
        <h1 style="font-family: 'Arvo', serif;"> Similar Product</h1>
    </div>
    <div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
    <div id="similar_product">
    <div class="item">
      <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocart" class="addme">
    Add to Cart
    <span class="cart-item"></span>
    </button>
    </div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>

    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocartt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/4.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  
</div>
</div>
</div>
</div>
</section>
<section class="top_deal">
    <div class="walnut_head_title">
        <h1 style="font-family: 'Arvo', serif;"> Same Color Product</h1>
    </div>
    <div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
    <div id="sameColorProd">
    <div class="item">
      <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocart" class="addme">
    Add to Cart
    <span class="cart-item"></span>
    </button>
    </div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>

    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
    <div class="page-wrapper">
    <button id="addtocartt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/4.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/6.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/7.webp" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocarttttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
  <div class="item">
    <div class="walnut_wish">
      <img src="icon/wish.png">
    </div>
    <img src="product/8.jpg" alt="Shirt">
    <div class="walnut_price">
    <span>Rs 3000/-</span>
    </div>
        <div class="page-wrapper">
    <button id="addtocartttttttt">
    Add to Cart
    <span class="cart-item"></span>
    </button>
</div>
  </div>
</div>
</div>
</div>
</div>
</section>
<div class="container-fluid wn_prodBan">
    <div class="row"></div>
      <!-- <img class="col-lg-12" src="images/pinkban.jpg"> -->
  </div>

     <?php include('include/footer.php') ?>
	<script src="js/main.js"></script>


  </body>
</html>