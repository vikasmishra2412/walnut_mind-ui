 <!doctype html>
<html lang="en">
  <head>

    <?php include('include/head.php') ?>
  </head>
  <body>
    <?php include('include/header.php') ?>

    <div class="container">
   <div class="row wish_con">
       <div class="col-12">
           <ul class="wish_ul">
             <li class="wish_li wish_title_li">
                <h4 class="wish_title">My Wishlist (24)</h4>
            </li>

             <li class="row wish_li">
              <div class="col-3 wislist_prodImg">
                   <img src="product/4.jpg" alt="">
              </div>
              <div class="col-6 wishlist_prodTitle">
                  <h5 class="wishlist_proName">Linen shirt Regular Fit</h5>
                  <h4 class="cart_price">&#x20B9; 2000/-</h4>
              </div>
              <div class="col-2 wish_bin">
                  <span class="wish_binIcon"><i class="fas fa-trash"></i></span>
              </div>
             </li>
             <li class="row wish_li">
              <div class="col-3 wislist_prodImg">
                   <img src="product/4.jpg" alt="">
              </div>
              <div class="col-6 wishlist_prodTitle">
                  <h5 class="wishlist_proName">Linen shirt Regular Fit</h5><br>
                  <h4 class="cart_price">&#x20B9; 2000/-</h4>
              </div>
              <div class="col-2">
                  <span class="wish_binIcon"><i class="fas fa-trash"></i></span>
              </div>
             </li>
             <li class="row wish_li">
              <div class="col-3 wislist_prodImg">
                   <img src="product/4.jpg" alt="">
              </div>
              <div class="col-6 wishlist_prodTitle">
                  <h5 class="wishlist_proName">Linen shirt Regular Fit</h5><br>
                  <h4 class="cart_price">&#x20B9; 2000/-</h4>
              </div>
              <div class="col-2">
                  <span class="wish_binIcon"><i class="fas fa-trash"></i></span>
              </div>
             </li>
             <li class="row wish_li">
              <div class="col-3 wislist_prodImg">
                   <img src="product/4.jpg" alt="">
              </div>
              <div class="col-6 wishlist_prodTitle">
                  <h5 class="wishlist_proName">Linen shirt Regular Fit</h5><br>
                  <h4 class="cart_price">&#x20B9; 2000/-</h4>
              </div>
              <div class="col-2">
                  <span class="wish_binIcon"><i class="fas fa-trash"></i></span>
              </div>
             </li>
           </ul>
       </div>
   </div>
</div>


    <?php include('include/footer.php') ?>

    
  </body>
</html>