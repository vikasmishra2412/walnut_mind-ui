 <!doctype html>
<html lang="en">
  <head>

    <?php include('include/head.php') ?>
  </head>
  <body>
    <?php include('include/header.php') ?>

    
    <div class="wallnut_cart">
        <div class="row wallnut_cartHead">
            <div class="col-lg-4">
            <h5>Product Name</h5>
            </div>
            <div class="col-lg-2"><h5>Details</h5></div>
            <div class="col-lg-3"><h5>Unit Price</h5></div>
            <div class="col-lg-3"><h5>Sub Total</h5></div>
        </div>
        <div class="row wn_productRow">
                <div class="col-lg-4 wallnut_imgContainer">
                    <div class="wallnut_img">
                        <img src="product/4.jpg">
                        <h5 class="wallnut_productName">Linen shirt Regular Fit</h5>
                    </div>
                </div>
                <div class="col-lg-2 pt-5" class="wallnut_cartProductDetail">
                    
                </div>
                <div class="col-lg-3 pt-5" class="wallnut_orderDetail">
                    <h3>Price : 2000/-</h3>
                    <h3 class="wallnut_cartQnt"> Quantity : 1</h3>
                </div>
                <div class="col-lg-3 pt-5" class="wallnut_orderDetail">
                    <h3>Price : 2000/-</h3>
                </div>
            </div>
            <div class="wallnut_dividerLine"></div>
          
           
          
    </div>

    <!-- mobile -->
    <div class="container">
    <div class="row wn_cartMobTable">
        <div class="col-12">
            <table>
                <tr class="wn_mobTR">
                    <td><h3 class="mth3">Product Name</h3></td>
                    <td><div class="wallnut_img">
                            <img src="product/4.jpg">
                            <h5 class="wallnut_productName mth5">Linen shirt Regular Fit</h5>
                        </div>
                    </td>
                </tr>
                <tr class="wn_mobTR">
                    <td><h3 class="mth3">Details</h3></td>
                        <td></td>
                </tr>
                <tr class="wn_mobTR">
                    <td><h3 class="mth3">Unit Price</h3></td>
                    <td> 
                        <h5 class="mth5">Price : 2000/-</h5>
                        <h5 class="wallnut_cartQnt mth5"> Quantity : 1</h5>
                    </td>
                </tr>
                <tr class="wn_mobTR">
                    <td><h3 class="mth3">Sub Total</h3></td>
                    <td><h5 class="mth5">Price : 2000/-</h5></td>
                </tr>
            </table>
         </div>
    </div>
    </div>

    <div class="wallnut_priceDetail">
        <h5>Price Details</h5>
        <div class="row ">
            <div class="txtL col-6">Total MRP</div>
            <div class="txtR col-6">&#x20B9; 2000/-</div>
        </div>
        <div class="row">
            <div class="txtL col-6">Discount</div>
            <div class="txtR col-6">&#x20B9; 0/-</div>
        </div>
        <div class="row">
            <div class="txtL col-6">Discount</div>
            <div class="txtR col-6">0%</div>
        </div>
        <div class="wallnut_dividerLine"></div>
        <div class="row">
            <div class="txtL col-6">Total</div>
            <div class="txtR col-6">&#x20B9; 2000/-</div>
        </div>
        <div class="wallnut_cartPlaceOrder">
            <h6>Have a promotion code?</h6>
            <a class="wallnut_checkoutBtn" href="checkout.php">Place Order</a>
        </div>
    </div>



    <?php include('include/footer.php') ?>

    
  </body>
</html>