 <!doctype html>
<html lang="en">
  <head>
    <?php include('include/head.php') ?>
  </head>
  <body>
    <?php include('include/header.php') ?>

    <div class="container pt-5">
         <div class="row">

            <!-- ********Dashboard********* -->
          <div class="mainDash col-sm-12 col-md-12 col-lg-2">
            <ul class="wn_dash">
                <li class="Dtitle">Dashboard</li>
                <li>Purchased Items</li>
                <li>Affilated Code</li>
                <li>Withdraw</li>
                <li>Order Tracking</li>
                <li>Favorite Seller</li>
                <li>Messages</li>
                <li>Tickets</li>
                <li>Disputes</li>
                <li>Edit Profile</li>
                <li>Reset Password</li>
                <li class="mb-3 botBorder">Logout</li>
            </ul>
          </div>

          <!-- *******Account Info******* -->
          <div class="ai_div ml-3 col-xs-12 col-lg-4">
            <h4 class="pb-3">Account Information</h4>
            <h6 class="textColler">USER</h6>
            <h6><strong>Email</strong> : user@gmail.com</h6>
            <h6><strong>Phone</strong> : 1233452251</h6>
            <h6><strong>Fax</strong> : 5646465646</h6>
            <h6><strong>City</strong> : Test City</h6>
            <h6><strong>Zip</strong> : 1231</h6>
            <h6><strong>Address</strong> : test address</h6>
          </div>
          <div class="ai_div ml-3 col-xs-12 col-lg-4">
              <h4 class="pb-3">My Balance</h4>
              <h5 class="textColler">Affiliate Bonus:</h5>
              <h6 class="textColler">&#x20B9; 225424.23</h6>
              <h5 class="textColler">Loyalty Points:</h5>
              <h6 class="textColler">2376 Points</h6>
              <a class="redBtn" href="#">REDEEM</a>
          </div>
          <div class="col-sm-12 col-md-12 col-lg-2">
              <a class="sellBtn" href="">Start Selling</a>
          </div>
          <div class="od_div bodDiv ml-3 p-2 pt-5 pb-5 col-sm-12 col-md-12 col-lg-4">
            <div class="totalO"><h4 class="cirTxtAlign">0</h4></div>
              <h4 class="pb-3 textColler">Total Order</h4>
              <h6>ALL TIME</h6>
          </div>
          <div class="p-2 pt-5 pb-5 bodDiv bodDivMB ml-3 col-sm-12 col-md-12 col-lg-4">
            <div class="pendingO"><h4 class="cirTxtAlign">1</h4></div>
              <h4 class="pb-3 textColler">Pending Order</h4>
              <h6>ALL TIME</h6>
          </div>
         </div>
    </div>




    <?php include('include/footer.php') ?>
	
  </body>
</html>