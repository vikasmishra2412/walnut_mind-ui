<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('include/head.php') ?>
</head>
<body>
<?php include('include/header.php') ?>

<section>
  <div class="walnut_store_list_banner">
    <img src="images/store.png"  class="w-100">
  </div>
</section>
<section class="walnut_filter">
  <div class="container">
    <div class="row mt-4">
      <div class="col-md-9">
        <div class="example">
        <form>
        <input type="search" class="searchbox walnut_box" name="q" autocomplete="off" placeholder="Search Your Store..." />
        <button type="submit" class="searchbutton"><i class="fa fa-search"></i></button>
        </form>
        </div>
      </div>
      <div class="col-md-3">
       <div class="walnut_filtr_button">
        <button>Filter</button>
       </div>
      </div>
    </div>
  </div>
</section>


<section class="list_of_store">
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12">
  <div id="walnut_store_list">
  <div class="item">
     
  </div>
  <div class="item walnut_logoround">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
</div>
</div>
</div>
</div>
</section>

<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="walnut_store_list_detail_product_adv">
          <img src="images/storedetail.jpg">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="list_of_store">
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12">
  <div id="walnut_store_list_two">
  <div class="item walnut_logoround">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
</div>
</div>
</div>
</div>
</section>


<section class="list_of_store">
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12">
  <div id="walnut_store_list_three">
  <div class="item walnut_logoround">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
</div>
</div>
</div>
</div>
</section>


<section class="walnut_store_detail_list">
  <div class="container-fluid">
    <div class="row">
      <div class="col-6">
        <div class="walnut_store_banner">
          <img src="images/nike.jpg">
        </div>
      </div>
      <div class="col-6">
        <div class="walnut_store_banner">
          <img src="images/zara.jpg">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="list_of_store">
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12">
  <div id="walnut_store_list_four">
  <div class="item walnut_logoround">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
</div>
</div>
</div>
</div>
</section>
<section class="list_of_store">
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12">
  <div id="walnut_store_list_five">
  <div class="item walnut_logoround">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
    <div class="item">
    <img src="store-image/s3.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s4.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
    <img src="store-image/s5.jpg">
    <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
  <div class="item">
   <img src="store-image/s7.jpg">
   <div class="walnut_prodetail">
      <div class="walnut_prologo">
      <img src="logo/s8l.png" class="round">
      </div>
      <h3><a href="store_detail.php">Go to Store <i class="fa fa-arrow-right" aria-hidden="true"></i></a></h3>
      <h4><a href="store_detail.php">Dorothy Perkins</a></h4>
      </div>
  </div>
</div>
</div>
</div>
</div>
</section>


<section>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="walnut_store_list_detail_product_adv">
          <img src="images/storedetail.jpg">
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="walnut_marquee">
  <div class="walnut_track">
    <div class="content">&nbsp;Walkommerce Store List, Walkommerce Store List, Walkommerce Store List, Walkommerce Store List, Walkommerce Store List, Walkommerce Store List, Walkommerce Store List, </div>
  </div>
</div>
</section>
<?php include('include/footer.php') ?>
</body>
</html>

